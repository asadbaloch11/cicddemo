package com.example.cicdDemo.Controllers;

import com.example.cicdDemo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class DemoController {

    @Autowired
    private DemoService demoService;

    @GetMapping("/abeySaalay")
    String testGet(){
        return "Maaf kerna, Ghussay me kabhi kabhi CI/CD kerleta hun";
    }

    @GetMapping("/cim")
    String testCim(){
        return  demoService.getNAme();
    }
}
